import * as React from 'react';
const Logo = () => {
    return (
        <div className="logo">
            <span className="first_logo">BE</span>
            <span className="second_logo" >FIT</span>
        </div>
    )
}

export default Logo